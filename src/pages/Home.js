 import Hightlights from '../components/Highlights';
 import Banner from '../components/Banner';
//  import FeaturedProducts from '../components/FeaturedProducts';

 import logo from '../images/logo.jpg';

export default function Home() {

	const data = {
		image: <img src={logo} alt="logo" width="150" height="120"/>,
		destination: "/products",
		label: "Shop Now!"
	}

	

	return (
		<>
			 <Banner data={data}/> 
			 
			 <Hightlights/> 
			 {/* <FeaturedProducts/> */}
			
		</>


	)
};