import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {
  const { productId } = useParams();
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [quantity, setQuantity] = useState(''); // Default quantity is set to 1

  useEffect(() => {
    // Fetch the product details
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then(res => res.json())
      .then(data => {
        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
      });
  }, [productId]);

  const handleQuantityChange = (event) => {
    const newQuantity = parseInt(event.target.value);
    setQuantity(newQuantity);
  };

  const purchase = () => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/purchase`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        productId: productId,
        quantity: quantity // Use the selected quantity
      })
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: 'Purchased Successfully',
            icon: 'success',
            text: 'You have Successfully purchased the item'
          });

          navigate('/products');
        } else {
          Swal.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please try again.'
          });
        }
      });
  };

  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body className="text-center">
              <img src="https://image.lexica.art/md/3e48a36e-3939-42f8-8973-86aa080f0fd1" alt="profile-pic" width="100" height="100" />
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {price}</Card.Text>

              <div className="d-flex justify-content-center align-items-center">
                <span className="me-2">Quantity:</span>
				<Form>
				    <Form.Label>User Name</Form.Label>
				    <Form.Control 
				    	type="number"
				    	min="1"
				    	value={quantity}
				    	onChange={handleQuantityChange}
				    />
				  </Form>

                {/* <input
                  type="number"
                  min="1"
                  value={quantity}
                  onChange={handleQuantityChange}
                /> */}
              </div>

              {user.id !== null ? (
                <Button id='purchase' variant="primary" onClick={purchase} className="mt-2">Purchase
                </Button>
              ) : (
                <Button as={Link} to="/login" variant="danger" className="mt-4">
                  Log in to Purchase
                </Button>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}