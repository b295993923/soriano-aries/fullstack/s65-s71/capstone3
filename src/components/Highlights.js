import {Row, Col, Card} from 'react-bootstrap';


export default function Highlights() {

	return (

		
		<Row className="mt-2 mb-5 text-center">
			<Col xs={12} md={4}>
			
				
			
					<Card className="card1 cardHighlight p-3 container-fluid">
					<Card.Body>
						<Card.Title>Shop in Your Own Convenient</Card.Title>
						<Card.Text>
						24/7 Accessibility: Our online marketplace is open round-the-clock, granting you the flexibility to shop at your preferred time. Whether it's a late-night craving for the latest gadgets, a midday quest for fashion finds, or a lazy Sunday browsing for home essentials!
						</Card.Text>
					</Card.Body>
					</Card>
			
			
			
			</Col>

			<Col xs={12} md={4}>
				<Card className="card1 cardHighlight p-3 container-fluid">
				  <Card.Body>
				    <Card.Title>Where Quality Meets Savings!</Card.Title>
				    <Card.Text>
                    Customer-Centric Approach: Your satisfaction is our top priority. Our user-friendly website is designed to make your shopping experience seamless and enjoyable. We value your feedback and continuously strive to enhance our services to meet your expectations.
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="card1 cardHighlight p-3 container-fluid">
				  <Card.Body>
				    <Card.Title>Unbelievable Savings Await!</Card.Title>
				    <Card.Text>
                    Trusted Brands, Unbeatable Prices: We partner with renowned brands to bring you quality products at prices that won't strain your budget. Rest assured that you'll be getting genuine, top-notch items that deliver on both performance and value.
				    </Card.Text>
				  </Card.Body>
				</Card>
			</Col>
		</Row>
		

	)
}
