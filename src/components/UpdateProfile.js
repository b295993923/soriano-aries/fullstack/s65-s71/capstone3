import React, { useState } from 'react';
import {Button, Container, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

const UpdateProfileForm = ({fetchDetails}) => {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  

  /*const handleInputChange = (event) => {
    const { name, value } = event.target;
    switch (name) {
      case 'firstName':
        setFirstName(value);
        break;
      case 'lastName':
        setLastName(value);
        break;
      case 'mobileNo':
        setMobileNo(value);
        break;
      default:
        break;
    }
  };*/

  const handleFormSubmit = async (e) => {
    e.preventDefault();

    // Update the profile API endpoint
    const apiUrl = `${process.env.REACT_APP_API_URL}/users/profile`;
    const token = localStorage.getItem('token');

    try {
      const response = await fetch(apiUrl, {
        method: 'PUT', // Use 'PUT' for updating the profile
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({
          username,
          email
         
        }),
      });

      if (response.ok) {
          Swal.fire({
          title: 'Success!',
          icon: 'success',
          text: 'User details Successfully Updated!'
      })
        console.log('Profile updated successfully!');
        refreshInput();
        fetchDetails();
      } else {
        Swal.fire({
          title: 'Error',
          icon: 'error',
          text: 'Please try again'
        })
        console.error('Failed to update profile.');
        refreshInput();
        fetchDetails();
      }
    } catch (error) {
      console.error('Error occurred while updating profile:', error);
    }
  };

  const refreshInput = () => {
    setUsername('');
    setEmail('');
    
  }

  return (
    <Form onSubmit={handleFormSubmit} className="form container text-center" id="updateProfile">
      <h1>Update Profile</h1>
      <div className="form-group">
        <label htmlFor="userName">User Name</label>
        <input
          type="text"
          className="form-control"
          id="userName"
          name="userName"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
          required
        />
      </div>

      <div className="form-group">
        <label htmlFor="email">Email</label>
        <input
          type="text"
          className="form-control"
          id="email"
          name="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
      </div>

      
        <Button id='update' type="submit" className="btn btn-primary mt-4 mb-4">Update Profile</Button>
      
      
      
    </Form>
  );
};

export default UpdateProfileForm;
