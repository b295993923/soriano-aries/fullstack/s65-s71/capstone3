import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

const ArchiveProduct = ({ productId, isActive, fetchData }) => {
  const archiveToggle = () => {
    // Fetch /archive from the backend
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`
      },
      body: JSON.stringify({
        isActive: !isActive
      })
    })

    // Add sweetalert for confirmation
    Swal.fire({
      title: "Success!",
      icon: "success",
      text:"Product successfully disabled",
      
    })

    // On successful response, fetch updated data using fetchData()
    fetchData();

  };

  const activateToggle = () => {
    // Fetch /activate from the backend
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/activate`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`
      },
      body: JSON.stringify({
        isActive: !isActive
      })
    })
    // Add sweetalert for confirmation
    Swal.fire({
      title: "Success!",
      icon: "success",
      text:"Product successfully enabled",
      
    })
    // On successful response, fetch updated data using fetchData()
    fetchData();

  };

  return (
    <div className='archive'>
      {isActive ? (
        <Button variant='danger' onClick={archiveToggle}>Archive</Button>
      ) : (
        <Button variant='success' onClick={activateToggle}>Activate</Button>
      )}
    </div>
  );
};

export default ArchiveProduct;
