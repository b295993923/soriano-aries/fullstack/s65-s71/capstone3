import React, { useState, useEffect } from 'react';
import {  Table } from 'react-bootstrap';
import EditProduct from './EditProduct';
import ArchiveProduct from './ArchiveProduct';


export default function AdminView({ productsData, fetchData }) {

  // State to store all courses 
  const [products, setProducts] = useState([]);

  // Getting the coursesData from the courses page
  useEffect(() => {
    const productsArr = productsData.map(product => (
      <tr key={product._id}>
        <td>{product._id}</td>
        <td>{product.name}</td>
        <td>{product.description}</td>
        <td>{product.price}</td>
        <td className={product.isActive ? "text-success" : "text-danger"}>
          {product.isActive ? "Available" : "Unavailable"}
        </td>
        <td><EditProduct product={product._id} fetchData={fetchData} /></td>
        <td>
        <ArchiveProduct
            productId={product._id}
            isActive={product.isActive}
            fetchData={fetchData}
          />          
        </td>
      </tr>
    ));

    setProducts(productsArr);

  }, [productsData])

  return (
    <>
      <h1 className="text-center my-4">Admin Dashboard</h1>

      <div>
      <Table striped bordered hover responsive>
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Availability</th>
            <th colSpan="2">Actions</th>
          </tr>
        </thead>

        <tbody>
          {products}
        </tbody>
      </Table>
      </div>

      
    </>

  )
}
