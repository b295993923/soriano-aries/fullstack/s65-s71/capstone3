import { useState, useEffect } from 'react';

import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}) {

    // console.log(props);
    // console.log(typeof props);
    // console.log(props.courseProp.name)

    // console.log(courseProp);

    // Destructured the courseProp object to access its properties.
    const {_id, name, description, price } = productProp; 

    // console.log(name);


    /*
        Use the state hook for this component to be able to store its state.
        States are used to keep track of the information related to individual components.


        Syntax:
            const [getterVar, setterFunc] = useState(initialGetterValue);
        
    */

    // const [count, setCount] = useState(0);
    // const [seats, setSeats] = useState(30)

    // // console.log(useState(0));

    // function enroll() {
    //     if(seats > 0) {
    //         setCount(count + 1)
    //         // console.log('Enrollees: ' + count);
    //         setSeats(seats - 1)
    //         // console.log('Seats: ' + seats);
    //     } 
    //     //else {
    //     //  alert("No more seats.")
    //     //}
    // };

    // // console.log('Enrollees: ' + count);
    // // console.log('Seats: ' + seats);

    // useEffect(() => {
    //     if (seats === 0) {
    //         alert("No more seats.")
    //     }
    // }, [seats]);



    return (
        
        <Row className='justify-content-md-center my-5'>
            <Col xs={12} md={6}>
                <Card className="cardHighlight p-3 my-3">
                    <Card.Body>
                        <img src="https://image.lexica.art/md/3e48a36e-3939-42f8-8973-86aa080f0fd1" alt="profile-pic" width="100" height="100" />
                        <Card.Title>{name}</Card.Title>
                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>PhP {price}</Card.Text>
                        <Button as={Link} to={`/products/${_id}`} variant="primary" id='viewButton'>View Details</Button>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
};

