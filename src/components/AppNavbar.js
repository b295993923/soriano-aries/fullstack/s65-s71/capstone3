import {useState, useContext} from 'react';
import {Navbar, Nav, Container} from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import logo from '../images/logo.png';
import '../App.css';


export default function AppNavbar() {

	// const [user, setUser] = useState(localStorage.getItem("token"));

	const { user } = useContext(UserContext);
	const [isCollapsed, setIsCollapsed] = useState(false);

	console.log(user);

	
	
	


	return(

		<Navbar expand="lg" className="bg-body-tertiary p-3">
		      <Container fluid className="appNavBar">
			  	<Navbar.Brand as={Link} to="/"><img src={logo} alt="logo" width="80" height="50"  /></Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          <Nav className="ms-auto">
		            <Nav.Link as={NavLink} to="/" >Home</Nav.Link>
		            <Nav.Link as={NavLink} to="/products" >Products</Nav.Link>
					
		            
		            {
		            	(user.id !== null && user.isAdmin === true) ?
		            	  <>
		            	  	<Nav.Link as={NavLink} to="/addProduct" >Add Products</Nav.Link>
							<Nav.Link as={NavLink} to="/updateUserAdmins" >Admin</Nav.Link>
							<Nav.Link as={NavLink} to="/orders/allOrder" >Total Orders</Nav.Link>
		            		<Nav.Link as={NavLink} to="/logout" >Logout</Nav.Link>
								            		
		            	  </>
		            	  :
		            	(user.id !== null) ?
		            	  <>
		            	  	<Nav.Link as={NavLink} to="/profile" >Profile</Nav.Link>
							<Nav.Link as={NavLink} to="/orders/userOrders" >Order</Nav.Link>
		            		<Nav.Link as={NavLink} to="/logout" >Logout</Nav.Link>	            		
		            	  </>
		            	  :
		            	  <>
		            	  	<Nav.Link as={NavLink} to="/login" >Login</Nav.Link>
		            	  	<Nav.Link as={NavLink} to="/register" >Register</Nav.Link>
		            	  </>
		            }

		          </Nav>
		        </Navbar.Collapse>
		      </Container>
		    </Navbar>

	)

};