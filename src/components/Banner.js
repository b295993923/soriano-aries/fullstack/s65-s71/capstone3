import { Row, Col} from 'react-bootstrap';
import { Link } from "react-router-dom";

	// data is inside the curly bracket because data is an object
export default function Banner({data}) {

	const {title, content, destination,label} = data;

	return (

		<Row>
			<Col className="p-5 text-center">
				{data.image}
				<p className='content my-3'>{content}</p>
				<Link className="btn" id='shop' to={destination}> {label} </Link>
			</Col>
		</Row>


	)
};
