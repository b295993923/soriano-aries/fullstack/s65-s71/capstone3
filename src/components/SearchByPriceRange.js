import React, { useState } from "react";

const ProductSearch = () => {
  const [minPrice, setMinPrice] = useState("");
  const [maxPrice, setMaxPrice] = useState("");
  const [searchResults, setSearchResults] = useState([]);
  const [error, setError] = useState(null);

  const handleSearch = () => {
    setError(null);

    const isInvalidInput = isNaN(minPrice) || isNaN(maxPrice);
    if (isInvalidInput) {
      setError("Please enter valid numbers for the price range.");
      return;
    }

    // Optional: Add additional input validation for prices
    const minPriceValue = Number(minPrice);
    const maxPriceValue = Number(maxPrice);

    if (minPriceValue < 0 || maxPriceValue < 0) {
      setError("Prices cannot be negative.");
      return;
    }

    if (minPriceValue >= maxPriceValue) {
      setError("Minimum price must be less than maximum price.");
      return;
    }

    fetch(`${process.env.REACT_APP_API_URL}/products/searchByPrice`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ minPrice: minPriceValue, maxPrice: maxPriceValue }),
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok.");
        }
        return response.json();
      })
      .then((data) => {
        setSearchResults(data);
        console.log(searchResults)
      })
      .catch((error) => {
        setError("Error fetching data. Please try again later.");
        console.error("Error fetching data:", error);
      });
  };

  return (
    <div className="form container my-5">
      <h2>Search Products by Price Range</h2>
      {error && <div className="alert alert-danger">{error}</div>}
      <div className="form-row">
        <div className="col mb-2">
          <input
            type="number"
            className="form-control"
            placeholder="Minimum Price"
            value={minPrice}
            onChange={(e) => setMinPrice(e.target.value)}
            min="0" // Optional: Set a minimum value for the input
          />
        </div>
        <div className="col">
          <input
            type="number"
            className="form-control"
            placeholder="Maximum Price"
            value={maxPrice}
            onChange={(e) => setMaxPrice(e.target.value)}
            min="0" // Optional: Set a minimum value for the input
          />
        </div>
        <div className="col my-3 text-center">
          <button id="search" className="btn btn-primary" onClick={handleSearch}>
            Search
          </button>
        </div>
      </div>
      {searchResults.length > 0 && (
        <div className="mt-3 mb-5">
          <h4>Search Results:</h4>
          <ul className="list-group">
            {searchResults.map((product) => (
              <li key={product.id} className="list-group-item">
                {product.name} - ${product.price}
              </li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
};

export default ProductSearch;

